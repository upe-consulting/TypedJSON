/* tslint:disable:no-unused-variable */
import {expect} from 'chai';
import * as jsondiffpatch from 'jsondiffpatch';
import {suite, test} from 'mocha-typescript';

import {TypedJSON} from '../src/typed-json';
import {
  BigNode,
  Company,
  Employee,
  Graph,
  GraphGrid,
  Investor,
  IPoint,
  Node,
  PartTimeEmployee,
  Person,
  SmallNode,
} from './classes';

const DiffPatcher = jsondiffpatch.create();


//noinspection JSUnusedLocalSymbols
@suite('TypedJSON')
class TypedJSONTest {

  private static randPortType() {
    const types = [
      'string',
      'integer',
      'float',
      'boolean',
      'void',
    ];

    return types[Math.floor(Math.random() * types.length)];
  }

  //noinspection JSMethodCanBeStatic
  @test
  public 'polymorphism'() {

    // Create a Company.
    const company = new Company();
    company.name = 'Json Types';

    switch (Math.floor(Math.random() * 4)) {
      case 0:
        company.owner = new Employee('John', 'White', 240000, new Date(1992, 5, 27));
        break;

      case 1:
        company.owner = new Investor('John', 'White', 1700000);
        break;

      case 2:
        company.owner = new PartTimeEmployee('John', 'White', 160000, new Date(1992, 5, 27));
        (
          company.owner as PartTimeEmployee
        ).workHours = Math.floor(Math.random() * 40);
        break;

      default:
        company.owner = new Person('John', 'White');
        break;
    }

    // Add employees.
    for (let j = 0; j < 20; j++) {
      if (Math.random() < 0.2) {
        const newPartTimeEmployee = new PartTimeEmployee(
          `firstname_${j}`,
          `lastname_${j}`,
          Math.floor(Math.random() * 80000),
          new Date(Date.now() - Math.floor(Math.random() * 80000)),
        );

        newPartTimeEmployee.workHours = Math.floor(Math.random() * 40);

        company.employees.push(newPartTimeEmployee);
      } else {
        company.employees.push(new Employee(
          `firstname_${j}`,
          `lastname_${j}`,
          Math.floor(Math.random() * 80000),
          new Date(Date.now() - Math.floor(Math.random() * 80000)),
        ));
      }
    }

    TypedJSON.config({
      enableTypeHints: true,
    });

    const json = TypedJSON.stringify(company);
    const reparsed = TypedJSON.parse(json, Company);

    return expect(DiffPatcher.diff(company, reparsed)).to.be.undefined;

  }

  //noinspection JSMethodCanBeStatic
  @test
  public 'polymorphism abstract class'() {

    const graph = new Graph();

    for (let i = 0; i < 20; i++) {
      let node: Node;

      if (Math.random() < 0.25) {
        const bigNode = new BigNode();

        bigNode.inputs = [
          TypedJSONTest.randPortType(),
          TypedJSONTest.randPortType(),
          TypedJSONTest.randPortType(),
        ];
        bigNode.outputs = [
          TypedJSONTest.randPortType(),
          TypedJSONTest.randPortType(),
        ];

        node = bigNode;
      } else {
        const smallNode = new SmallNode();

        smallNode.inputType = TypedJSONTest.randPortType();
        smallNode.outputType = TypedJSONTest.randPortType();

        node = smallNode;
      }

      node.name = `node_${i}`;

      if (i === 0) {
        graph.root = node;
      } else {
        graph.nodes.push(node);
      }
    }

    TypedJSON.config({
      enableTypeHints: true,
    });

    const json = TypedJSON.stringify(graph);
    const clone = TypedJSON.parse(json, Graph);

    return expect(DiffPatcher.diff(graph, clone)).to.be.undefined;

  }

  //noinspection JSMethodCanBeStatic
  @test
  public 'polymorphism interface'() {
    const graph = new GraphGrid();

    for (let i = 0; i < 20; i++) {
      let point: IPoint;

      if (Math.random() < 0.25) {
        const bigNode = new BigNode();

        bigNode.inputs = [
          TypedJSONTest.randPortType(),
          TypedJSONTest.randPortType(),
          TypedJSONTest.randPortType(),
        ];
        bigNode.outputs = [
          TypedJSONTest.randPortType(),
          TypedJSONTest.randPortType(),
        ];

        point = bigNode;
      } else {
        const smallNode = new SmallNode();

        smallNode.inputType = TypedJSONTest.randPortType();
        smallNode.outputType = TypedJSONTest.randPortType();

        point = smallNode;
      }

      point.x = Math.random();
      point.y = Math.random();

      if (i === 0) {
        graph.root = point;
      } else {
        graph.points.push(point);
      }
    }

    TypedJSON.config({
      enableTypeHints: true,
    });

    const json = TypedJSON.stringify(graph);
    const clone = TypedJSON.parse(json, GraphGrid);

    return expect(DiffPatcher.diff(graph, clone)).to.be.undefined;
  }

  //noinspection JSMethodCanBeStatic
  @test
  public 'polymorphism nested arrays'() {
    const graph = new Graph();

    for (let i = 0; i < 20; i++) {
      graph.smallItems.push([]);

      for (let j = 0; j < 8; j++) {
        const node = new SmallNode();

        node.name = `smallnode_${i}_${j}`;
        node.inputType = TypedJSONTest.randPortType();
        node.outputType = TypedJSONTest.randPortType();

        graph.smallItems[i].push(node);
      }
    }

    for (let i = 0; i < 20; i++) {
      graph.items.push([]);

      for (let j = 0; j < 8; j++) {
        let node: Node;

        if (Math.random() < 0.25) {
          const bigNode = new BigNode();

          bigNode.inputs = [
            TypedJSONTest.randPortType(),
            TypedJSONTest.randPortType(),
            TypedJSONTest.randPortType(),
          ];
          bigNode.outputs = [
            TypedJSONTest.randPortType(),
            TypedJSONTest.randPortType(),
          ];

          node = bigNode;
        } else {
          const smallNode = new SmallNode();

          smallNode.inputType = TypedJSONTest.randPortType();
          smallNode.outputType = TypedJSONTest.randPortType();

          node = smallNode;
        }

        node.name = `node_${i}_${j}`;

        graph.items[i].push(node);
      }
    }

    TypedJSON.config({
      enableTypeHints: true,
    });

    const json = TypedJSON.stringify(graph);
    const clone = TypedJSON.parse(json, Graph);

    return expect(DiffPatcher.diff(graph, clone)).to.be.undefined;
  }

  //noinspection JSMethodCanBeStatic
  @test
  public 'single class'() {
    const person = TypedJSON.parse('{ "firstName": "John", "lastName": "Doe" }', Person);

    expect(person.getFullname()).to.be.equal('John Doe');
    expect(person).is.instanceof(Person);
  }

}
