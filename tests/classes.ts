import {JsonMember, JsonObject} from '../src/';

@JsonObject()
export class Person {
  @JsonMember({type: String})
  public firstName: string;

  @JsonMember({type: String})
  public lastName: string;

  public getFullname() {
    return this.firstName + ' ' + this.lastName;
  }

  constructor(firstName?: string, lastName?: string) {
    if (firstName && lastName) {
      this.firstName = firstName;
      this.lastName = lastName;
    }
  }
}

@JsonObject()
export class Employee extends Person {
  @JsonMember({type: Number})
  public salary: number;

  @JsonMember({type: Date})
  public joined: Date;

  constructor(firstName?: string, lastName?: string, salary?: number, joined?: Date) {
    super(firstName, lastName);
    if (salary && joined) {
      this.salary = salary;
      this.joined = joined;
    }
  }
}

@JsonObject()
export class PartTimeEmployee extends Employee {
  @JsonMember({type: Number})
  public workHours: number;
}

@JsonObject()
export class Investor extends Person {
  @JsonMember({type: Number})
  public investAmount: number;

  constructor(firstName?: string, lastName?: string, investAmount?: number) {
    super(firstName, lastName);
    this.investAmount = investAmount || 0;
  }
}

@JsonObject({name: 'company', knownTypes: [PartTimeEmployee, Investor]})
export class Company {
  @JsonMember({type: String})
  public name: string;

  @JsonMember({elements: Employee})
  public employees: Employee[] = [];

  @JsonMember({type: Person})
  public owner: Person;
}

export interface IPoint {
  x: number;
  y: number;
}

export abstract class Node {
  @JsonMember({type: String})
  public name: string;
}

@JsonObject()
export class SmallNode extends Node implements IPoint {
  @JsonMember({type: Number})
  public x: number;

  @JsonMember({type: Number})
  public y: number;

  @JsonMember({type: String})
  public inputType: string;

  @JsonMember({type: String})
  public outputType: string;
}

@JsonObject()
export class BigNode extends Node implements IPoint {
  @JsonMember({type: Number})
  public x: number;

  @JsonMember({type: Number})
  public y: number;

  @JsonMember({elements: String})
  public inputs: string[] = [];

  @JsonMember({elements: String})
  public outputs: string[] = [];
}

@JsonObject({knownTypes: [BigNode, SmallNode]})
export class Graph {
  @JsonMember({elements: Node, refersAbstractType: true})
  public nodes: Node[] = [];

  @JsonMember({type: Node, refersAbstractType: true})
  public root: Node;

  @JsonMember({elements: {elements: Node}})
  public items: Node[][] = [];

  @JsonMember({elements: {elements: SmallNode}})
  public smallItems: SmallNode[][] = [];
}

@JsonObject({knownTypes: [BigNode, SmallNode]})
export class GraphGrid {
  @JsonMember({elements: Object, refersAbstractType: true})
  public points: IPoint[] = [];

  @JsonMember({type: Object, refersAbstractType: true})
  public root: IPoint;
}
