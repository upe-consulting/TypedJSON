[![build status](https://gitlab.com/upe-consulting/TypedJSON/badges/master/build.svg)](https://gitlab.com/upe-consulting/TypedJSON/commits/master)
[![coverage report](https://gitlab.com/upe-consulting/TypedJSON/badges/master/coverage.svg)](https://gitlab.com/upe-consulting/TypedJSON/commits/master) 

A fork from [John Weisz](https://github.com/JohnWeisz/TypedJSON) TypedJSON project.

# TypedJSON

Strong-typed JSON parsing and serializing for TypeScript with [decorators](https://github.com/Microsoft/TypeScript-Handbook/blob/master/pages/Decorators.md). Parse JSON into actual class instances. Recommended (but not required) to be used with [ReflectDecorators](https://github.com/rbuckton/ReflectDecorators), a prototype for an ES7 Reflection API for Decorator Metadata.

 - Parse regular JSON to typed class instances, safely
 - Seamlessly integrate into existing code with [decorators](https://github.com/Microsoft/TypeScript-Handbook/blob/master/pages/Decorators.md), ultra-lightweight syntax
---
 - [Wiki](https://gitlab.com/upe-consulting/TypedJSON/wikis/home)
 - [API documation](https://upe-consulting.gitlab.io/TypedJSON/api)
 - [Full coverage report](https://upe-consulting.gitlab.io/TypedJSON/coverage)

## Changes

 - Fix export issue
 - Split into multiple files
 - Unit tests
 - A TypedJSON decorator must know have a factory 

## TODOs

 - Test coverage > 85%
 - Refactoring
 - Extend documentation

## Install

```none
npm install --save @upe/typedjson
```
## Use

 1. Snap the @JsonObject decorator on a class
 2. Snap the @JsonMember decorator on properties which should be serialized and deserialized
 3. Parse and stringify with the TypedJSON class

```typescript
@JsonObject()
class Person {
    @JsonMember({type: String})
    firstName: string;

    @JsonMember({type: String})
    lastName: string;

    public getFullname() {
        return this.firstName + " " + this.lastName;
    }
}

var person = TypedJSON.parse('{ "firstName": "John", "lastName": "Doe" }', Person);

person instanceof Person; // true
person.getFullname(); // "John Doe"
```

[Learn more about decorators in TypeScript](https://github.com/Microsoft/TypeScript-Handbook/blob/master/pages/Decorators.md)

## License

TypedJSON is licensed under the MIT License.
